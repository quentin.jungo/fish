extends Control

@onready var resume_button = $CenterContainer/VBoxContainer/UnPauseButton

var is_paused = false
var status_before_pause


func _ready():
	self.visible = false

func _on_menu_button_pressed():
	SoundManager.click()
	if is_paused:
		_set_is_paused()
	Global.playing_status = Global.MENU
	Global.reinit()
	Global.go_to_menu.emit()
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")


func _input(event):
	if event.is_action_pressed("pause"):
		if !self.is_paused:
			resume_button.grab_focus()
		_set_is_paused()
	

func _set_is_paused():
	self.is_paused = !self.is_paused
	get_tree().paused = self.is_paused
	self.visible = is_paused
	if is_paused:
		status_before_pause = Global.playing_status
		Global.playing_status = Global.PAUSE
	else:
		Global.playing_status = status_before_pause
	#print("game paused: ", self.is_paused, Global.playing_status)


func _on_un_pause_button_pressed():
	SoundManager.click()
	_set_is_paused()


func _on_quit_button_pressed():
	SoundManager.click()
	get_tree().quit()
