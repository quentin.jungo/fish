extends Control

@onready var new_game_button = $ColorRect/CenterContainer/VBoxContainer/NewGameButton
@onready var dead_fish_label = $ColorRect/VBoxContainer2/Label
@onready var dead_fishes_list = $ColorRect/VBoxContainer2/FishesKilled

func _ready():
	Global.is_first_play = false
	Global.dead_fishes.map(func(fish_name): dead_fishes_list.add_text(fish_name); dead_fishes_list.add_text(" "))
	Global.playing_status = Global.MENU
	var _dead_fishes_amount = Global.dead_fishes.size()
	Global.reinit()
	SoundManager.death()
	new_game_button.grab_focus()
	var label_string = ""
	if _dead_fishes_amount == 0:
		label_string = "Congratulations ! You did not kill any fish."
	else:	
		label_string = "Here is the name of the %s fishes killed by your incompetence" % str(_dead_fishes_amount)
	dead_fish_label.text = label_string

		
func _on_new_game_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/level/ocean.tscn")


func _on_menu_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")

	
func _on_quit_button_pressed():
	SoundManager.click()
	TransitionManager.quit()
