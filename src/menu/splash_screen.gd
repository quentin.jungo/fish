extends Control

@onready var studio_scene = $StudioScene/AnimationPlayer 

func _ready():
	SoundManager.menu_music()
	studio_scene_play()


func _input(event):
	if Global.is_first_play: 
		return
	if event is InputEventJoypadButton or event is InputEventKey or event is InputEventMouseButton:
		TransitionManager.goto_scene("res://src/menu/start_menu.tscn")


func studio_scene_play():
	studio_scene.play("fade_in")
	studio_scene.queue("stay_studio")
	#print("end")

func _on_animation_player_animation_finished(_anim_name):
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")
