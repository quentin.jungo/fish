extends Control

@onready var back_button = $GridContainer/GoBackButton
@onready var music_slider = $GridContainer/MusicSliderContainer/MusicSlider
@onready var sfx_slider = $GridContainer/SoundSliderContainer/SoundSlider

func _ready():
	music_slider.value = SoundManager.music_volume * 100
	sfx_slider.value = SoundManager.sfx_volume * 100
	back_button.grab_focus()

func _on_go_back_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")


func _on_music_slider_value_changed(value):
	#print("set musci volume")
	SoundManager.music_volume = value / 100

func _on_sound_slider_value_changed(value):
	SoundManager.sfx_volume = value / 100
