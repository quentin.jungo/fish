extends Control
@export var velocity = Vector2(0.0, -15.0)

func _ready():
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color(modulate.r, modulate.g, modulate.b, 0.0), 2.0).set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN)
	tween.finished.connect(_on_tween_finished)


func _on_tween_finished():
	self.queue_free()
