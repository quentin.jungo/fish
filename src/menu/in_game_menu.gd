extends CanvasLayer

var death_log_scene = preload("res://src/menu/dead_fish_log.tscn")
@onready var childs = self.get_child_count()


func _ready():
	Global.death.connect(_on_death)


func _on_death(name_ : String, color_ : Color):
	var death_log = death_log_scene.instantiate()
	var messages = self.get_child_count() - childs
	death_log.modulate = color_
	death_log.find_child("Label").text = "%s is dead" % name_
	death_log.find_child("Label").position += Vector2(0.0, -15.0 * messages)
	add_child(death_log)

