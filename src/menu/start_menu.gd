extends Control

@onready var playbutton = $VBoxContainer/HBoxContainer/VBox/PlayButton

func _ready():
	SoundManager.menu_music()
	playbutton.grab_focus()

func _on_play_button_pressed():
	SoundManager.click2()
	TransitionManager.goto_scene("res://src/level/ocean.tscn")
	Global.playing_status = Global.INTRO
	
	
func _on_setting_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/menu/settings.tscn")


func _on_credits_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/menu/credits.tscn")

func _on_quit_button_pressed():
	SoundManager.click()
	TransitionManager.quit()
