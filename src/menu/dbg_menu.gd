extends CenterContainer

@onready var freeze_toggle: CheckButton = $VBoxContainer/Container/Freeze

func _ready():
	Global.dbg_freeze = freeze_toggle.button_pressed
	freeze_toggle.button_up.connect(func(): Global.dbg_freeze = freeze_toggle.button_pressed)
