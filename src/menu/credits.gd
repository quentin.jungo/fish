extends Control

@onready var back_button = $VBoxContainer/GoBackButton

func _ready():
	back_button.grab_focus()

func _on_go_back_button_pressed():
	SoundManager.click()
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")
