@tool
extends Node2D
class_name Algue

const long = 50

var nb_segments: int = 10
var height: float
var width: float
var color: Color

var speeds = [0.0]
var masses = [1.0]
var t = PI / 2 * randf()


func create(height_: float, width_: float, color_: Color):
	height = height_
	width = width_
	color = color_

	get_child(0).default_color = color
	get_child(0).width = width
	get_child(2).timeout.connect(_on_timer_timeout)
	
	nb_segments = int(height / long * max(randfn(1, 0.15), 0.5))
	var p = [Vector2.ZERO]
	var shape = Vector2(0, -long)
	
	for i in range(1, nb_segments):
		var child = RigidBody2D.new()
		var collision_shape = CollisionShape2D.new()
		var segment_shape = SegmentShape2D.new()
		segment_shape.b = shape
		collision_shape.shape = segment_shape
		child.add_child(collision_shape)

		child.position = i * shape
		var pin_join = PinJoint2D.new()
		child.add_child(pin_join)
		
		child.gravity_scale = $Segments/Segment0.gravity_scale
		child.mass = $Segments/Segment0.mass
		child.set_collision_layer_value(1, false)
		child.set_collision_mask_value(1, false)
		child.set_collision_layer_value(2, true)
		child.set_collision_mask_value(2, true)
	
		$Segments.add_child(child)
		pin_join.node_a = child.get_path()
		pin_join.node_b = $Segments.get_children()[i - 1].get_path()
		
		speeds.push_back(0.0)
		masses.push_back(1.0)
		p.push_back(i * shape)
	$Shape.points = p
	if !Engine.is_editor_hint():
		$Timer.start()


func _physics_process(delta):
	if Engine.is_editor_hint():
		return
	t += delta
	var segments = $Segments.get_children()
	
	var p = $Shape.points
	for i in range(nb_segments):
		p[i] = segments[i].position
		var force = sin(t + PI * i / nb_segments + randfn(0, 1)) * (float(i) / nb_segments)
		segments[i].apply_central_impulse(Vector2.RIGHT * force)
	$Shape.points = p


func _on_timer_timeout():
	var value = not $Segments.get_child(0).get_collision_layer()
	for segment in $Segments.get_children():
		segment.set_collision_layer_value(2, value)
		segment.set_collision_mask_value(2, value)
	$Timer.wait_time = max(randfn(0.8, 0.5), 0.1)
	$Timer.start()
