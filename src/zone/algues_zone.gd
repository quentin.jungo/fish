@tool extends Node2D
class_name AlgueZone

var algue_scene = preload("res://src/zone/algue.tscn")

@export var nb_algues = 10:
	set(new_nb_algues):
		nb_algues = new_nb_algues
		if Engine.is_editor_hint():
			_add_all_algues()

@export var spacing = 40:
	set(new_spacing):
		spacing = new_spacing
		var delta = spacing * Vector2.from_angle(rotation)
		for i in range(get_child_count()):
			get_child(i).position = delta * i

@export var height = 300:
	set(new_height):
		height = new_height
		if Engine.is_editor_hint():
			_add_all_algues()

@export var width = 20:
	set(new_width):
		width = new_width
		for algue in get_children():
			algue.get_child(0).width = width

@export var color = Color.DARK_GREEN:
	set(new_color):
		color = new_color
		for algue in get_children():
			algue.get_child(0).default_color = color

const SPAN = 1000
var _spawned = false

func _ready():
	if Engine.is_editor_hint():
		_add_all_algues()

func _process(_delta):
	if Engine.is_editor_hint():
		return

	var cam_pos = Global.camera.position
	var inside = global_position.x - SPAN < cam_pos.x and global_position.x + SPAN * 2 > cam_pos.x
	if inside:
		if not _spawned:
			_add_all_algues()
		_spawned = true
	if not inside:
		if _spawned:
			get_children().map(func(algue): algue.queue_free())
		_spawned = false
	
	
func _add_all_algues():
	for child in get_children():
		remove_child(child)
		child.queue_free()

	for i in range(nb_algues):
		var algue: Algue = algue_scene.instantiate()
		if Engine.is_editor_hint():
			algue.set_owner(get_parent().owner)
		add_child(algue)
		algue.create(height, width, color)
		algue.position = spacing * Vector2.RIGHT * i
