extends Node2D

@onready var lightArea: Area2D = $LightArea2D
@onready var light: Light2D = $ProjectingLight
@onready var buzz_sfx = $light_buzz
@onready var kill_sfx = $light_kill
@onready var switch_on_sfx = $light_switch_on
@onready var switch_off_sfx = $light_switch_off
var timer: Timer = Timer.new()
var fishesInligthArea: Array[Node2D]
const VISIBLE_TIME_S = 2
const HIDE_TIME_S = 0.5


func _ready():
	lightArea.set_collision_mask_value(1, false)
	lightArea.body_entered.connect(_on_body_entered_light_area)
	add_child(timer)
	timer.timeout.connect(_on_timeout)
	timer.wait_time = VISIBLE_TIME_S
	timer.start()


func _on_timeout():
	light.visible = !light.visible 
	if light.visible:
		lightArea.set_collision_mask_value(1, false) 
		timer.wait_time = VISIBLE_TIME_S
		switch_on_sfx.play()
		buzz_sfx.play()
	else:
		lightArea.set_collision_mask_value(1, true) 
		timer.wait_time = HIDE_TIME_S
		switch_off_sfx.play()
		buzz_sfx.stop()
	timer.start()


func _on_body_entered_light_area(body: Node2D):
	if body is Fish:
		_silent_kill(body)


func _silent_kill(fish: Fish):
	var fishes_in_light = Global.lightZone.get_overlapping_bodies().filter(func(body): return body is Fish)
	if not fishes_in_light.has(fish):
		fish.kill()
		if not kill_sfx.is_playing():
			kill_sfx.play()
