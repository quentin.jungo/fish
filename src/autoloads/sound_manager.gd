extends Node

var music_volume = 1.:
	set(new):
		new = clamp(new, 0., 1.)
		AudioServer.set_bus_volume_db(1, remap(new, 0., 1., -30., 0.))
		music_volume = new
		Global.save()

var sfx_volume = 1.:
	set(new):
		new = clamp(new, 0., 1.)
		AudioServer.set_bus_volume_db(5, remap(new, 0., 1., -30., 0.))
		sfx_volume = new
		Global.save()

func _ready():
	Global.go_to_menu.connect(_stop_intro)
	Global.end_intro.connect(_stop_intro)
	AudioServer.set_bus_volume_db(2, 0)
	AudioServer.set_bus_volume_db(3, -80)
	AudioServer.set_bus_volume_db(7, -80)
	AudioServer.set_bus_volume_db(8, -80)


func _stop_intro():
	$Voices/IntroP1.stop()
	$Voices/IntroP2.stop()
	$Musics/Piano.stop()


func compass_tuto():
	$Voices/Compass.play()


func start_music():
	$Musics/level_abysse.play()
	$Musics/level_abysse_LP.play()
	$Musics/level_abysse_LP2.play()
	$Musics/shark_music.play()
	$Musics/KrakenZone.play()
	$Musics/HoleZone.play()



func is_music_playing() -> bool:
	return $Musics/level_abysse.playing


func death():
	$Musics/level_abysse.stop()
	$Musics/level_abysse_LP.stop()
	$Musics/shark_music.stop()

func click():
	$ButtonClick.play()

func click2():
	$ButtonClick2.play()

func menu_music():
	$Musics/level_abysse.stop()
	$Musics/level_abysse_LP.stop()
	$Musics/level_abysse_LP2.stop()
	$Musics/shark_music.stop()
	$Musics/KrakenZone.stop()
	$Musics/HoleZone.stop()
	AudioServer.set_bus_volume_db(2, 0)
	AudioServer.set_bus_volume_db(3, -80)
	AudioServer.set_bus_volume_db(7, -80)
	AudioServer.set_bus_volume_db(8, -80)
	if not $Musics/Menu.playing:
		$Musics/Menu.play()

func stop_menu_music():
	$Musics/Menu.stop()

func get_clan():
	$GetClan.play()

func end():
	$Musics/level_abysse.stop()
	$Musics/level_abysse_LP.stop()
	$Musics/level_abysse_LP2.stop()
	$Musics/shark_music.stop()
	$Musics/KrakenZone.stop()
	$Musics/HoleZone.stop()
	$Musics/Win.play()
	await $Musics/Win.finished
	# $Musics/End.play()
	$Voices/End.play()
