extends Node

func is_one_action_pressed(actions_name: Array[String]) -> bool:
	for action_name in actions_name:
		if Input.is_action_just_pressed(action_name):
			return true
	return false
