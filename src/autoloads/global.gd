extends Node

enum {
	MENU,
	PAUSE,
	INTRO,
	INTRO_END,
	SWIMMING,
	END, # Taking by the stream
}

signal go_to_menu
signal end_intro
signal end_intro_fish_move
signal death(String, Color)
signal win
signal clan_spawned
signal clan_collected


var fishes_alive_amount: int = START_FISH_AMOUNT
var playing_status = MENU
var camera: Camera2D
var lightZone: Area2D
var dead_fishes : Array[String] = []
var dbg_freeze = false
## The high zone where you get killed very quickly
var is_in_fishing_zone = false 
var is_first_play: bool
var fish_names = FISH_NAMES.duplicate()
var clan_names = CLAN_NAME.duplicate()
var clan_colors = CLAN_COLORS.duplicate()
## {<global_position>, <not_collected_bool>}
var cousin_clans = {}

const cousins_per_clan: int = 20
const PPREFS_PATH = &"res://player_prefs3.cfg"
const FISHING_ZONE_HEIGHT = 4_000
const VISIBLE_AREA_RADIUS_PHOSPHORE = 300 # and anglefish
const STAY_TIME_PHOSPHORE_S = 4 # and anglefish
const START_FISH_AMOUNT = 100

func _ready():
	var player_prefs = ConfigFile.new()
	var initied: bool = false
	if player_prefs.load(PPREFS_PATH) != OK:
		initied = true
		player_prefs = _init_pprefs()
	is_first_play = player_prefs.get_value("gameplay", "first_play", true)
	SoundManager.music_volume = player_prefs.get_value("gameplay", "music_volume", 1)
	SoundManager.sfx_volume = player_prefs.get_value("gameplay", "sfx_volume", 1)
	if not initied and is_first_play:
		is_first_play = false
		player_prefs.set_value("gameplay", "first_play", false)
		player_prefs.save(PPREFS_PATH)

## call after a death to reload the game !!
func reinit():
	fish_names = FISH_NAMES.duplicate()
	clan_names = CLAN_NAME.duplicate()
	clan_colors = CLAN_COLORS.duplicate()
	fishes_alive_amount = START_FISH_AMOUNT
	# camera = null
	# lightZone = null
	dead_fishes.clear()
	dbg_freeze = false
	is_in_fishing_zone = false 
	cousin_clans = {}

func _init_pprefs():
	var player_prefs = ConfigFile.new()
	player_prefs.set_value("gameplay", "first_play", true)
	player_prefs.set_value("gameplay", "music_volume", 1.)
	player_prefs.set_value("gameplay", "sfx_volume", 1.)
	player_prefs.save(PPREFS_PATH)
	return player_prefs


func save():
	var player_prefs = ConfigFile.new()
	player_prefs.set_value("gameplay", "first_play", true)
	player_prefs.set_value("gameplay", "music_volume", SoundManager.music_volume)
	player_prefs.set_value("gameplay", "sfx_volume", SoundManager.sfx_volume)
	player_prefs.save(PPREFS_PATH)

func pick_clan_color():
	var col
	if clan_colors.size() == 0:
		col = Color.DEEP_PINK
	else:
		col = clan_colors.pick_random()
		clan_colors.remove_at(clan_colors.find(col))
	return col

func pick_clan_name():
	var foo
	if clan_names.size() == 0:
		foo = &"Foobar"
	else:
		foo = clan_names.pick_random()
		clan_names.remove_at(clan_names.find(foo))
	return foo

const CLAN_COLORS = [Color.AQUA, Color.LIGHT_CORAL, Color.SANDY_BROWN, Color.AQUAMARINE, Color.GOLD, Color.ORCHID, Color.TEAL, Color.LIGHT_STEEL_BLUE, Color.GREEN_YELLOW]

const CHUNKS = [
	preload("res://src/level/chunks/000.tscn"),
	preload("res://src/level/chunks/001.tscn"),
	preload("res://src/level/chunks/002.tscn"),
	preload("res://src/level/chunks/003.tscn"),
	preload("res://src/level/chunks/004.tscn"),
	preload("res://src/level/chunks/005.tscn"),
	preload("res://src/level/chunks/006.tscn"),
	preload("res://src/level/chunks/007.tscn"),
	preload("res://src/level/chunks/008.tscn"),
	preload("res://src/level/chunks/009.tscn"),
]

const CLAN_NAME: Array[StringName] = [
	&"Mc Fisherboys",
	&"Fishson",
	&"Phliosh",
	&"De Fish-noble",
	&"Mc Pesca",
	&"Sir Fishalot",
	&"Dark lords"
]

const FISH_NAMES: Array[StringName] = [
	&"Arnaud",
	&"Quentin",
	&"Niki",
	&"Yani",
	&"Julien",
	&"Amory",
	&"Paul",
	&"Emile",
	&"Jean",
	&"John",
	&"Michael",
	&"Billy",
	&"Miley",
	&"Emily",
	&"Mick",
	&"Michelle",
	&"Jane",
	&"Jimmy",
	&"Josh",
	&"Ben",
	&"Seb",
	&"Annouk",
	&"Boubakar",
	&"Sandrine",
	&"Eddy",
	&"Nissa",
	&"Luke",
	&"Mahé",
	&"Loic",
	&"Aurelien",
	&"Guillaume",
	&"Bruno",
	&"Eloise",
	&"Martha",
	&"Marie",
	&"George",
	&"Pedro",
	&"Pablo",
	&"Edouardo",
	&"Mohamed",
	&"Thomas",
	&"Lucas",
	&"Ted",
	&"Tess",
	&"Tobias",
	&"Erika",
	&"René",
	&"Romuald",
	&"Roger",
	&"Rick",
	&"Tibald",
	&"Thibault",
	&"Timothé",
	&"Ibrahim",
	&"Patrick",
	&"Denis",
	&"Jordie",
	&"Jeremi",
	&"Markus",
	&"Monique",
	&"Mireille",
	&"Nathan",
	&"Nils",
	&"Vadim",
	&"Robert",
	&"Bob",
	&"Elias",
	&"Fernando",
	&"Francisco",
	&"Mira",
	&"Sasha",
	&"Ana",
	&"Charlotte",
	&"Gaelle",
	&"Matheo",
	&"Daniela",
	&"Pierre",
	&"Michel",
	&"Hélene",
	&"Marcus",
	&"Marco",
	&"Abygael",
	&"Jerome",
	&"Gabriel",
	&"Armand",
	&"Stefane",
	&"Philippe",
	&"Théo",
	&"Jaques",
	&"Henrik",
	&"Noa",
	&"Barnabé",
	&"Clitorine",
	&"Toncarysmme",
	&"Jessica",
	&"Joana",
	&"Thierry",
	&"Josué",
	&"Magali",
	&"Titouan",
	&"Marcelo",
	&"Kevin",
	&"Enzo",
	&"Vincent",
	&"Jack",
	&"Jean-Pierre",
	&"Jean-Luc",
	&"Jean-Jaques",
	&"Jean-Sebastien",
	&"Jean-Michel",
	&"Jaquie",
	&"Geralt",
	&"Ronald",
	&"Simus",
	&"Simon",
	&"Gaston",
	&"Gaspard",
	&"Genviève",
	&"Valentin",
	&"Valerie",
	&"Joël",
	&"Baptiste",
	&"Vladimir",
	&"Sam",
	&"Samuel",
	&"Didier",
	&"Xavier",
	&"Alain",
	&"Tim",
	&"Bill",
	&"Marty",
	&"Doc",
	&"Mitch",
	&"Krin",
	&"Kira",
	&"Lisa",
	&"Lise",
	&"Lisandre",
	&"Noam",
	&"Joao",
	&"Lana",
	&"Mayleen",
	&"Satheen",
	&"Sandrine",
	&"Alan",
	&"Leonie",
	&"Tommy",
	&"Flibuste",
	&"Foulque",
	&"Adama",
	&"Manon",
	&"Celine",
	&"Zinedine",
	&"Vivianne",
	&"Manu",
	&"Emmanuel",
	&"Lee",
	&"Ching",
	&"Ming",
	&"Stella",
	&"Edwin",
	&"Hugo",
	&"Hugues",
	&"Marcelin",
	&"Marcel",
	&"Jean-Louis",
	&"Louis",
	&"Corinne",
	&"Celestin",
	&"Melinda",
	&"Emma",
	&"Margoulin",
	&"Khir",
	&"Nadim",
	&"Nadège",
	&"Noémie",
	&"Lila",
	&"Lola",
	&"Lolita",
	&"Loan",
	&"Lebron",
	&"Baltus",
	&"Greg",
	&"Baldwin",
	&"Sméagol",
	&"Téagol",
	&"Gandoulf",
	&"Yohanes",
	&"Johan",
	&"Gretta",
	&"Daryl",
	&"Morty",
	&"Samir",
	&"Dean",
	&"Albus",
	&"Severus",
	&"Peter",
	&"Wendy",
	&"Pomme",
	&"Aurèle",
	&"Jaqueline",
	&"Micheline",
	&"Mélodie",
	&"Sylvie",
	&"Jean-Jaques",
	&"Malo",
	&"Sergei",
	&"Sigmund",
	&"Zlatan",
	&"Bizantin",
	&"Ursula",
	&"Guenièvre",
	&"Arthur",
	&"Floyd",
	&"Sophie",
	&"Sophia",
	&"Emmeline",
	&"Éli",
	&"Ella",
	&"Morganne",
	&"Yvettes",
	&"Huguette",
	&"Raimond",
	&"Rodri",
	&"Pedreti",
	&"Nello",
	&"Manilla",
	&"Harrisson",
	&"Lewis",
	&"Charles",
	&"Tanner",
	&"Gautier",
	&"Timéo",
	&"Timo",
	&"Jumbo",
	&"Artemis",
	&"Tadej",
	&"Slumdog",
	&"Silenna",
	&"Séréna",
	&"Tyson",
	&"Mac",
	&"Finigan",
	&"Esteban",
	&"Sia",
	&"Tao",
	&"Ninon",
	&"Nyxon",
	&"Nelly",
	&"Bastien",
	&"Alexis",
	&"Jordan",
	&"Jake",
	&"Tains",
	&"Tahissia",
	&"Hasina",
	&"Tael",
	&"Tony",
	&"Edison",
	&"Diego",
	&"Toad",
	&"Diogo",
	&"Charlie",
	&"Carla",
	&"Clara",
	&"Maia",
	&"Dylan",
	&"Brody",
	&"Zoe",
	&"Tacha",
	&"Tara",
	&"Yutani",
	&"Lucy",
	&"Queeny",
	&"Norbert",
	&"Bonnie",
	&"Églantine"
]