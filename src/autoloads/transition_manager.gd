extends Node

var current_scene = null

func _ready():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() - 1)
	Transition.play_black_to_normal()

func quit():
	get_tree().quit()
	
func goto_scene(path):
	Transition.play_normal_to_black()
	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path):
	get_tree().change_scene_to_file(path)
	Transition.play_black_to_normal()
	# It is now safe to remove the current scene
	
