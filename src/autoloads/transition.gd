extends CanvasLayer

signal transitioned

func _ready():
	self.hide()
	Global.win.connect(_win)
	
func play_normal_to_black() :
	self.show()
	$AnimationPlayer.play("normal_to_black")
	
func play_black_to_normal() :
	self.show()
	$AnimationPlayer.play("fade_to_normal")
	
func _on_animation_player_animation_finished(_anim_name):
	self.hide()

func _win():
	self.show()
	await create_tween().tween_method(func(t): $ColorRect.color.a = t, 0., 1, 15).finished
	TransitionManager.goto_scene("res://src/menu/start_menu.tscn")
	Global.playing_status = Global.MENU
	self.hide()
	$ColorRect.color.a = 1

