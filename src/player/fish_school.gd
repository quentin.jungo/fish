extends Node2D
class_name FishSchool


enum {
	MOUSE,
	JOYPAD,
}

@onready var movement_sound = find_child("AudioStreamPlayer")
@onready var phosphore_light = $Camera/PhosphoreLight
var fish_preload = preload("res://src/player/fish.tscn")

var input_mode = JOYPAD
var light_is_in_obstacle: Array[Node2D] = []
## Is the light we control blocked ? eg: Behind a wall.
var is_light_freeze: bool = false
var joy_position: Vector2 = Vector2.ZERO

const MOVEMENT_VOLUME = 2
const SPEED = 220_000
const MOUSE_TORQUE = 35000
const PRAYING_RADIUS = 180
const SCHOOL_RADIUS = 150
const CONTROL_RADIUS = 300
const SHINE_TIME_S = 12



func _ready():
	phosphore_light.energy = 0
	Global.camera = $Camera
#	Global.fish_names = Global.FISH_NAMES.duplicate()
# TODO: znichola refactor to remove this  refactor to move this stuff out of here

	var circles: int = 2
	var peripeter: float = Global.START_FISH_AMOUNT / float(circles)
	for c in range(circles):
		for i in range(peripeter):
			var new_fish = fish_preload.instantiate()
			var angle = TAU * i / peripeter
			new_fish.position = (PRAYING_RADIUS + c * 18) * Vector2.from_angle(angle)
			new_fish.linear_velocity = 1000 * Vector2.from_angle(angle + TAU / 3)
			new_fish.rotation = angle
			$fishes.add_child(new_fish)
	
	movement_sound.volume_db = -10
	await Global.end_intro_fish_move
	$fishes.get_children().map(func(fish): fish.is_swimming = true)
	

func _input(event):
	if Utils.is_one_action_pressed(
		["pad1_down", "pad1_left", "pad1_right", "pad1_up",
		"pad2_down", "pad2_left", "pad2_right", "pad2_up", 
		"pad_a", "pad_b"]
		):
		input_mode = JOYPAD
	elif event is InputEventMouseMotion:
		input_mode = MOUSE


func _process(delta):
	var all_fishes = $fishes.get_children().filter(func(body): return body is Fish)
	Global.fishes_alive_amount = all_fishes.size()
	if Global.fishes_alive_amount <= 0 :
		get_tree().change_scene_to_file("res://src/menu/death_menu.tscn")



	## Set the background color
	var height_pourcent = min($Camera.position.y * -1 / 15_000, 1)
	$Camera/CanvasLayer/ColorRect.color.b = max(height_pourcent, 0)
	
	## Set is_in_fishing_zone
	Global.is_in_fishing_zone = $Camera.position.y < 0 and abs($Camera.position.y) > Global.FISHING_ZONE_HEIGHT

	## Get the light position (and direction)
	is_light_freeze = !light_is_in_obstacle.is_empty()
	if input_mode == JOYPAD:
		# Pad Gauche (rapide)
		var pad1_vector = Input.get_vector("pad1_left", "pad1_right", "pad1_up", "pad1_down")
		pad1_vector = pad1_vector.normalized() * (pad1_vector.length() ** 1.6)
		# Pad Droit (lent)
		var pad2_vector = Input.get_vector("pad2_left", "pad2_right", "pad2_up", "pad2_down")
		pad2_vector = 0.5 * pad2_vector.normalized() * (pad2_vector.length() ** 1.6)
		# Pad 1 overrite Pad 2
		var pad_vector = pad2_vector if not pad2_vector.is_zero_approx() else pad1_vector
		if $Camera: joy_position = CONTROL_RADIUS * pad_vector + $Camera.position

	## Get the camera position
	var position_sum = Vector2.ZERO
	var speed_sum = Vector2.ZERO
	for fish in all_fishes:
		position_sum += fish.position
		if fish is Fish:
			speed_sum += fish.linear_velocity
	var center = position_sum / all_fishes.size()
	var avreage_speed = speed_sum / all_fishes.size()
	var smoothing = (1 - exp(-avreage_speed.length() / 500))
	var zoom_factor = exp(-avreage_speed.length() / 1500) * smoothing + 1 - smoothing
	$Camera.zoom *= 0.98 + 0.02 * 1.5 * zoom_factor / $Camera.zoom.length()
	$Camera.offset = 0.99 * $Camera.offset - 0.01 * 2000 * (1 - zoom_factor) * avreage_speed.normalized()
	$Camera.position = center
	
	## Fishes movement
	var input_direction = _compute_dir()
	var move_speed = SPEED * input_direction.length() #SPEED * (input_direction.length() + IDLE_SPEED)
	movement_sound.volume_db = -10 + input_direction.length() * 10

	if Global.dbg_freeze:
		return
	for fish in all_fishes:
		if fish.is_swimming:
			var school_radius = SCHOOL_RADIUS * Global.fishes_alive_amount / float(Global.START_FISH_AMOUNT)
			var fish_to_center = center - fish.position
			var far = fish_to_center.length() / school_radius

			# Goes towards center of school
			fish.apply_central_force(delta * 60 * school_radius * far ** 3 * fish_to_center.normalized())
			# Rotates in direction of the input (mouse or joystick)
			fish.rotate_towards(delta * MOUSE_TORQUE, input_direction)
			# Move forward
			if Global.playing_status == Global.END:
				fish.apply_central_force((delta * SPEED * 2) * Vector2.RIGHT)
			else:
				fish.move_forward(delta * move_speed)
		else:
			var far = -fish.position.length() / float(PRAYING_RADIUS / 2.)
			# Stay within radius
			fish.apply_central_force(delta * 60 * PRAYING_RADIUS / 2. * far ** 5. * fish.position.normalized())
			# Goes towards perimeter
			fish.apply_central_force(delta * 30_000 * fish.position.normalized())
			# Rotates
			fish.apply_central_force(delta * 50_000 * Vector2.from_angle(fish.rotation - PI / 4))
			fish.apply_torque(delta * -8000)


## Returns the unified input from 0 to 1 Vector2
func _compute_dir() -> Vector2:
	if Global.playing_status == Global.END:
		return Vector2.RIGHT
	var direction: Vector2
	match input_mode:
		MOUSE:
			direction = get_global_mouse_position() - $Camera.position - position
		JOYPAD:
			direction = joy_position - $Camera.position
		_:
			#print("Unsuported Input !")
			direction = Vector2.ZERO
	return direction.limit_length(CONTROL_RADIUS) / CONTROL_RADIUS


func shine():
	await create_tween().tween_property(phosphore_light, "energy", 1., 1.5).finished
	await get_tree().create_timer(SHINE_TIME_S).timeout
	create_tween().tween_property(phosphore_light, "energy", 0., 0.5)
