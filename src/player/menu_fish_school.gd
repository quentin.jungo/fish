extends Node2D


enum {
	MOUSE,
	JOYPAD,
}

var fish_preload = preload("res://src/player/fish.tscn")

var input_mode = JOYPAD
var joy_position: Vector2 = Vector2.ZERO

const SPEED = 220_000
const MOUSE_TORQUE = 35000
const SCHOOL_RADIUS = 150
const CONTROL_RADIUS = 700



func _ready():
	var circles: int = 4
	var peripeter: float = Global.START_FISH_AMOUNT / float(circles)
	for c in range(circles):
		for i in range(peripeter):
			var new_fish = fish_preload.instantiate()
			var angle = TAU * i / peripeter
			new_fish.position = (60 + c * 30) * Vector2.from_angle(angle)
			new_fish.linear_velocity = 1000 * Vector2.from_angle(angle + TAU / 3)
			new_fish.rotation = angle
			$fishes.add_child(new_fish)
	
	for fish in $fishes.get_children():
		fish.is_swimming = true
	

func _input(event):
	if Utils.is_one_action_pressed(
		["pad1_down", "pad1_left", "pad1_right", "pad1_up",
		"pad2_down", "pad2_left", "pad2_right", "pad2_up", 
		"pad_a", "pad_b"]
		):
		input_mode = JOYPAD
	elif event is InputEventMouseMotion:
		input_mode = MOUSE


func _process(delta):
	var all_fishes = $fishes.get_children().filter(func(body): return body is Fish)
	
	if input_mode == JOYPAD:
		# Pad Gauche (rapide)
		var pad1_vector = Input.get_vector("pad1_left", "pad1_right", "pad1_up", "pad1_down")
		pad1_vector = pad1_vector.normalized() * (pad1_vector.length() ** 1.6)
		# Pad Droit (lent)
		var pad2_vector = Input.get_vector("pad2_left", "pad2_right", "pad2_up", "pad2_down")
		pad2_vector = 0.5 * pad2_vector.normalized() * (pad2_vector.length() ** 1.6)
		# Pad 1 overrite Pad 2
		var pad_vector = pad2_vector if not pad2_vector.is_zero_approx() else pad1_vector
		joy_position = CONTROL_RADIUS * pad_vector

	## Get the camera position
	var position_sum = Vector2.ZERO
	var speed_sum = Vector2.ZERO
	for fish in all_fishes:
		position_sum += fish.position
	var center = position_sum / all_fishes.size()
	#joy_position = center
	
	## Fishes movement
	var input_direction = _compute_dir()
	var school_fish_to_center = (input_direction * CONTROL_RADIUS - center).limit_length(CONTROL_RADIUS) / CONTROL_RADIUS
	var move_speed = SPEED * school_fish_to_center.length()

	if Global.dbg_freeze:
		return
	
	for fish in all_fishes:
		var school_radius = SCHOOL_RADIUS * Global.fishes_alive_amount / float(Global.START_FISH_AMOUNT)
		var fish_to_center = input_direction * CONTROL_RADIUS - fish.position
		var far = fish_to_center.length() / school_radius
		
		# Goes towards center of school
		fish.apply_central_force(delta * 60 * school_radius * far * fish_to_center.normalized())
		# Rotates in direction of the input (mouse or joystick)
		fish.rotate_towards(delta * MOUSE_TORQUE, fish_to_center)
		# Move forward
		if Global.playing_status == Global.END:
			fish.apply_central_force((delta * SPEED * 2) * Vector2.RIGHT)
		else:
			fish.move_forward(delta * move_speed)


## Returns the unified input from 0 to 1 Vector2
func _compute_dir() -> Vector2:
	if Global.playing_status == Global.END:
		return Vector2.RIGHT
	var direction: Vector2
	match input_mode:
		MOUSE:
			direction = get_global_mouse_position() - position
		JOYPAD:
			direction = joy_position
		_:
			#print("Unsuported Input !")
			direction = Vector2.ZERO
	return direction.limit_length(CONTROL_RADIUS) / CONTROL_RADIUS
