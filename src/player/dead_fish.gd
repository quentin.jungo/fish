class_name DeadFish
extends Node2D

@onready var fishes_dead_sfx = [$fish_dead0, $fish_dead1, $fish_dead2, $fish_dead3, $fish_dead4, $fish_dead5, $fish_dead6, $fish_dead7]

func _ready(): 
	var foo = fishes_dead_sfx.pick_random()
	foo.seek(clamp(randfn(0, 0.2), 0, 0.2))
	foo.play()
	foo.pitch_scale = randfn(1.0, 0.2)

func _on_timer_timeout():
	queue_free()
