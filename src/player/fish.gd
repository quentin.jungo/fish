class_name Fish
extends RigidBody2D

# Swimming or Praying
var is_swimming = false
var fish_name : String
@onready var repulse_area = $RepulseArea
@onready var repulse_distance = $RepulseArea/CollisionShape2D.shape.radius
var dead_fish = preload("res://src/player/dead_fish.tscn")
var stunned_fish = preload("res://src/player/stunned_fish.tscn")
var oscillation = TAU / pulsation * randf()
const REPULSE_FORCE = 10_000
const pulsation = 0.00008

func move_forward(force: float):
	apply_central_force(force * Vector2.from_angle(rotation - PI / 2))
	oscillation += force * randfn(1, 0.2)
	apply_torque((100 + linear_velocity.length() * 5) * sin(pulsation * oscillation))

func rotate_towards(torque: float, direction: Vector2):
	apply_torque(torque * Vector2.from_angle(rotation - PI / 2).angle_to(direction))

func set_color(color : Color):
	$AnimatedSprite2D.modulate = color

func _ready():
	fish_name = get_fish_name()


func _physics_process(delta):
	# Repultion de courte distance
	for voisin in repulse_area.get_overlapping_bodies():
		if voisin != self && voisin is Fish:
			var delta_fish = position - voisin.position
			var repultion = (1 - delta_fish.length() / repulse_distance)
			apply_central_force(delta * REPULSE_FORCE * repultion * delta_fish.normalized())
	
	# (Attraction and Alignment is made with the attraction to mouse in fish_school)


func get_fish_name():
	return Global.fish_names.pick_random()

func kill():
	Global.death.emit(self.fish_name, $AnimatedSprite2D.modulate)
	var dead_body = dead_fish.instantiate()
	dead_body.position = position
	get_parent().add_child(dead_body)
	# #print(fish_name, " has perished")
	Global.dead_fishes.append(fish_name)
	queue_free()

# creates a new stunned fish, which will decay into a dead fish
func stun():
	Global.death.emit(self.fish_name, $AnimatedSprite2D.modulate)
	var stunned_body = stunned_fish.instantiate()
	stunned_body.position = position
	stunned_body.rotation = rotation
	get_parent().add_child(stunned_body)
	#print(fish_name, " has been stunned")
	Global.dead_fishes.append(fish_name)
	queue_free()
	return stunned_body
