extends Area2D

var is_in = false
@export var sharks: Array[Shark] = []
const MIN_DB = -30.0

func _ready():
	for shark in sharks:
		shark.gone.connect(_one_shark_gone)
	body_entered.connect(_on_body_entered)

##
func transition(t):
	AudioServer.set_bus_volume_db(3, t)
	AudioServer.set_bus_volume_db(2, MIN_DB - t)

##
func _on_body_entered(body):
	if not is_in and body is Fish:
		if body.find_parent("fish_clan") is Clan:
			return
		#printerr("in shark")
		is_in = true
		await create_tween().tween_method(transition, MIN_DB, 0.0, 1).finished
		## await the zone to be empty of fishes
		while not get_overlapping_bodies().filter(func(body): return body is Fish).is_empty():
			await get_tree().create_timer(3).timeout
		await create_tween().tween_method(transition, 0.0, MIN_DB, 1).finished
		is_in = false

##
func _one_shark_gone(shark):
	sharks.erase(shark)
	if sharks.is_empty():
		#printerr("out shark")
		await create_tween().tween_method(transition, 0.0, MIN_DB, 1).finished
		queue_free()
