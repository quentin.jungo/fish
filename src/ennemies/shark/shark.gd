extends Path2D
class_name Shark


signal gone(Shark)


@onready var attack_area = $shark_attack_zone
@onready var decetionRect = $shark_attack_zone/CollisionShape2D
@onready var mouth = $shark_mover/mouth
@onready var sound = $shark_mover/AudioStreamPlayer2D
@onready var follower = $PathFollow2D
@export var flip_shark = false
var is_attacking : bool = false


func _ready():
	$shark_mover/shark.flip_v = flip_shark
	$shark_mover/shark.modulate.a = 0
	attack_area.body_entered.connect(_on_entered)
	mouth.body_entered.connect(_on_mouth_entered)


func _on_entered(body:Node2D):
	if not is_attacking and body is Fish:
		is_attacking = true
		# #print("attack, ", follower.progress_ratio)
		if !sound.is_playing():
			sound.play()
		attack_area.queue_free()
		var tween = create_tween().set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
		tween.parallel().tween_property(follower, "progress_ratio", 0.99, 1)
		tween.parallel().tween_method(func(t): $shark_mover/shark.modulate.a = t, 0., 1., 0.1)
		await tween.finished
		await create_tween().tween_method(func(t): $shark_mover/shark.modulate.a = t, 1., 0., 0.1).finished
		gone.emit(self)
		sound.reparent(get_parent())
		queue_free()
		if sound.playing:
			await sound.finished
		sound.queue_free()	



func _on_mouth_entered(body: Node2D):
	if body is Fish:
		body.kill()
