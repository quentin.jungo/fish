extends Node2D

@onready var light = $Light
@onready var anglerfish = $Anglerfish
@onready var mouth_area = $MouthArea
@onready var visible_area = $VisibleArea

var is_comming = false


func _ready():
	anglerfish.modulate.a = 0
	mouth_area.body_entered.connect(_on_body_entered)
	visible_area.body_entered.connect(_on_visible_entered)
	visible_area.get_child(0).shape.radius = Global.VISIBLE_AREA_RADIUS_PHOSPHORE


func _on_body_entered(body):
	if not is_comming and body is Fish:
		$AudioStreamPlayer2D.play()
		$AudioStreamPlayer2D.pitch_scale = randf_range(0.8, 2) 

		is_comming = true
		await create_tween().tween_method(func(t): 
			anglerfish.modulate.a = t
			anglerfish.scale = (4. + 2. * t) * Vector2(1, 1)
			, 0., 1., 1).finished
		var doomed = mouth_area.get_overlapping_bodies().filter(func(body): return body is Fish)
		doomed.map(func(fish): fish.kill())
		await get_tree().create_timer(1).timeout
		await create_tween().tween_method(func(t): 
			anglerfish.modulate.a = t
			light.modulate.a = t
			anglerfish.scale = (4. + 2. * t) * Vector2(1, 1)
			, 1., 0., 0.5).finished
		if $AudioStreamPlayer2D.playing:
			await $AudioStreamPlayer2D.finished
		queue_free()

## THIS CODE IS DUPLICATE IN phosphore.gd !!!
func _on_visible_entered(body: Node2D):
	if body is Fish:
		visible_area.queue_free()
		await get_tree().create_timer(Global.STAY_TIME_PHOSPHORE_S).timeout
		await create_tween().tween_method(func(t): modulate.a = t, 1., 0., 0.1).finished
		if $AudioStreamPlayer2D.playing:
			await $AudioStreamPlayer2D.finished
		queue_free()
