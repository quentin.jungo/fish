extends Node2D
class_name FishNet

@onready var net: Sprite2D = $Fishnet
@onready var area: Area2D = $Fishnet/Area2D
const SCALE_FACTOR = 9
const SCALE_TIME_S = 1.2
const IADEIN_TIME_S = 0.5

func _ready():
	net.modulate.v = 0
	net.modulate.a = 0
	$AudioStreamPlayer2D.play()
	await create_tween().tween_method(func(t): net.modulate.a = t, 0., 1., IADEIN_TIME_S).finished
	_start()

func _start():
	await create_tween().tween_method(_get_bigger, 0., 1., SCALE_TIME_S).finished
	var captured_fishes = area.get_overlapping_bodies().filter(func(body): return body is Fish)
	captured_fishes.map(func(fish): fish.kill())
	if $AudioStreamPlayer2D.playing:
		await $AudioStreamPlayer2D.finished
	queue_free()

func _get_bigger(t: float):
	net.modulate.v = t
	net.scale = t * Vector2(SCALE_FACTOR, SCALE_FACTOR)
	