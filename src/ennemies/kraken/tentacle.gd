extends RayCast2D

enum {
	IDLE,
	CATCHING,
}

const nb_segments = 11
const G = 50_000
const TORQUE = 120_000_000 * 60
const IDLE_ANGLE = 15
const CATCH_ANGLE = 45

@export var mirrored: bool = false
@onready var segment_length = $Segments/Segment0/CollisionShape2D.shape.b.length()
@onready var idle_angle = deg_to_rad(IDLE_ANGLE if mirrored else -IDLE_ANGLE)
@onready var catch_angle = deg_to_rad(-CATCH_ANGLE if mirrored else CATCH_ANGLE)
var sound: AudioStreamPlayer2D
var angle = 0
var state = IDLE
var is_cycling = false


func _ready():
	if randi() % 2:
		sound = $Sound1
	else:
		sound = $Sound2

	var p = [Vector2.ZERO]
	var segment_shape = segment_length * target_position.normalized()

	var shape = $Segments/Segment0/CollisionShape2D.shape.duplicate()
	shape.a = -segment_shape / 6
	shape.b = segment_shape
	$Segments/Segment0/CollisionShape2D.shape = shape
	add_exception($Segments/Segment0)
	
	for i in range(1, nb_segments):
		var child = $Segments/Segment0.duplicate()
		shape = $Segments/Segment0/CollisionShape2D.shape.duplicate()
		shape.a = -segment_shape / 6
		shape.b = segment_shape
		child.get_child(0).shape = shape
		child.position = i * segment_shape
		child.freeze = false
		
		var pin_join = PinJoint2D.new()
		child.add_child(pin_join)
		add_exception(child)
		$Segments.add_child(child)
		
		pin_join.node_a = child.get_path()
		pin_join.node_b = $Segments.get_children()[i - 1].get_path()
		
		p.push_back(i * segment_shape)
	p.push_back(nb_segments * segment_shape)
	$Shape.points = p


func _physics_process(delta):
	if is_colliding() and not is_cycling:
		state = CATCHING
		is_cycling = true
		$TimerCatch.start()
		if not sound.playing:
			sound.play()
	
	var segments = $Segments.get_children()
	
	# Apply tentacle gravity (in direction of the ray)
	for i in range(nb_segments):
		segments[i].apply_central_force(delta * G * target_position.normalized())
	
	match state:
		IDLE:
			angle = 0.99 * angle + 0.01 * idle_angle
		CATCHING:
			angle = 0.95 * angle + 0.05 * catch_angle
	
	for i in range(1, nb_segments):
		var segment = segments[i]
		var k = float(nb_segments - i) / nb_segments
		var v1 = Vector2.from_angle(segments[i].rotation)
		var m = 1 - exp(-k)
		var v2 = Vector2.from_angle(segments[i - 1].rotation + (2 - m) * angle)
		segment.apply_torque(delta * TORQUE * m * v1.angle_to(v2) ** 3)
	
	
	var p = $Shape.points
	for i in range(nb_segments):
		p[i] = segments[i].position
	var last = segments[nb_segments - 1]
	p[nb_segments] = last.position + last.get_child(0).shape.b.rotated(last.rotation)
	$Shape.points = p


func _on_timer_catch_timeout():
	state = IDLE
	$TimerIdle.start()


func _on_timer_idle_timeout():
	is_cycling = false
