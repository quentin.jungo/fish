extends Node2D

func _ready():
	$Voice.play()
	$Voice.finished.connect(func(): $Voice.play())

	# Add exeptions to raycasts of tentacles for other tentacles
	var segments = []
	for tentacle in $Tentacles.get_children():
		segments.append_array(tentacle.get_child(1).get_children())
	for tentacle in $Tentacles.get_children():
		for segment in segments:
			tentacle.add_exception(segment)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var fishes_in_mouth = $Area2D.get_overlapping_bodies().filter(func(body): return body is Fish)
	if fishes_in_mouth.size() == 0:
		$AnimatedSprite2D.stop()
	else:
		$AnimatedSprite2D.play()
	
	for fish in fishes_in_mouth:
		var fish_to_mouth =get_global_position() - fish.get_global_position()
		fish.apply_central_force(delta * 5_000 * fish_to_mouth)
		if fish_to_mouth.length() < 50:
			fish.kill()


