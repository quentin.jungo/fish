extends Polygon2D

@onready var arms = $arms
var arm_preload = preload("res://src/ennemies/monster/arm.tscn")


func _ready():
	# reset setting for edditor
	if scale != Vector2(1, 1):
		#print("WARNING: don't scale the monster, resetting to zero")
		scale = Vector2(1, 1)

	# setup the collision shape in code
	var area = Area2D.new()
	color.a = 0
	var col = CollisionPolygon2D.new()
	col.polygon = polygon
	area.body_entered.connect(_on_body_endeterd)
	area.add_child(col)
	add_child(area)
	
	# setup the arm-s
	for i in range(0, 10):
		arms.add_child(arm_preload.instantiate())


func _on_body_endeterd(body):
	if body is Fish and randf() > 0.8:
		var arm = get_next_arm()
		if arm:
			arm.launch(body)


func get_next_arm():
	for arm in arms.get_children():
		if arm.is_ready():
			return arm
	return null
