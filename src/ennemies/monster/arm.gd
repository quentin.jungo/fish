extends Node2D

@onready var line = $Line2D
@onready var startup_timer = $startupTimer
@onready var grab_sfx = $AudioStreamPlayer2D

var target_fish: Fish
var stunned_fish: StunnedFish

const SPEED = 15 * 60
const rot = 0.6


func launch(fish: Fish):
	target_fish = fish
	startup_timer.start()

func _process(delta):
	# Trying to catch
	if !startup_timer.is_stopped() and not (target_fish == null or target_fish.is_queued_for_deletion()):
		var last_point = line.points[-1]
		var delta_last = last_point - (Vector2.ZERO if line.points.size() == 1 else line.points[-2])
		var fish_position = target_fish.position * global_transform
		var delta_new = (fish_position - last_point).limit_length(delta * SPEED)
		line.add_point(last_point + delta_new.rotated(-delta_last.angle_to(delta_new) * rot))
		# Stunt
		if (fish_position - last_point).length() < delta * SPEED:
			stunned_fish = target_fish.stun()
			startup_timer.stop()
			grab_sfx.play()
			grab_sfx.position = last_point
			grab_sfx.pitch_scale = randfn(1.0, 0.2)
	
	# Pulling back
	if startup_timer.is_stopped():
		var last_index = line.points.size() - 1
		if last_index != 0:
			line.remove_point(last_index)
			if stunned_fish:
				stunned_fish.position = line.points[-1] * global_transform.inverse()
		elif stunned_fish:
			stunned_fish.queue_free()
			stunned_fish = null
			return

func is_ready():
	return startup_timer.is_stopped() && line.points.size() == 1

