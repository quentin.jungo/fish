extends Path2D

@onready var puffer = $puffer_fish
@onready var follower = $PathFollow2D

# Called when the node enters the scene tree for the first time.
func _ready():
	var tween = create_tween().set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	tween.set_loops()
	tween.tween_property(follower, "progress_ratio", 1, 50)
