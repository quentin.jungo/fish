extends AnimatableBody2D

const CHARGING_TIME_S = 1
const TIME_AFTER_DEATH = 0.5

@onready var boom_area = $BoomArea
@onready var detect_area = $DetectArea
@onready var boom_particles = $BoomArea/CPUParticles2D
@onready var animation_prep: AnimatedSprite2D = $AnimatedPrep
@onready var animation_explosion: AnimatedSprite2D = $AnimatedExplosion
var is_charging = false


func _ready():
	animation_explosion.hide()
	boom_particles.set_emitting(false)
	detect_area.body_entered.connect(_on_detect_entered)


func _on_detect_entered(body:Node2D):
	if body is Fish and not is_charging:
		$AudioStreamPlayer2D.play()
		is_charging = true
		animation_prep.play()
		await animation_prep.animation_looped
		animation_prep.queue_free()
		animation_explosion.show()
		animation_explosion.play()
		create_tween().set_ease(Tween.EASE_OUT).tween_method(func(t): animation_explosion.scale = Vector2(t, t), 3., 20., 0.6)
		await animation_explosion.animation_looped
		var doomed = boom_area.get_overlapping_bodies().filter(func(body): return body is Fish)
		doomed.map(func(fish): fish.kill())
		hide()
		if $AudioStreamPlayer2D.playing:
			await $AudioStreamPlayer2D.finished	
		queue_free()
