extends Node2D

@onready var collect_area: Area2D = $CollectArea
@onready var visible_area: Area2D = $VisibleArea

var taken = false

func _ready():
	collect_area.body_entered.connect(_on_collect_entered)
	visible_area.body_entered.connect(_on_visible_entered)
	visible_area.get_child(0).shape.radius = Global.VISIBLE_AREA_RADIUS_PHOSPHORE


func _on_collect_entered(body: Node2D):
	if not taken and body is Fish:
		taken = true
		$Take.play()
		# #print("take")
		var fish_school = body.find_parent("FishSchool")
		if fish_school is FishSchool:
			fish_school.shine()
		else:
			printerr("THE PHOSPHORE DIDNT GET THE FISHSHCHOOL !")
		hide()
		create_tween().tween_property($angler_light/Sound, "volume_db", -40., 2.)
		await $Take.finished

		queue_free()

## THIS CODE IS DUPLICATE IN angler.gd !!!
func _on_visible_entered(body: Node2D):
	if body is Fish:
		visible_area.queue_free()
		await get_tree().create_timer(Global.STAY_TIME_PHOSPHORE_S).timeout
		await create_tween().tween_method(func(t): modulate.a = t, 1., 0., 0.1).finished
		queue_free()
