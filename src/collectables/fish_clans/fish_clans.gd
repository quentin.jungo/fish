extends Node2D
class_name Clan

const BASE_VOLUME = 2.
var fish_preload = preload("res://src/player/fish.tscn")
var old_position = Vector2(0.0, 0.0)
var clan_name
var all_collected = false
@onready var praying_radius = $Radius.shape.radius


func _ready():
	$Voice.play()
	$Voice.volume_db = BASE_VOLUME
	$Voice.finished.connect(func(): $Voice.play())
	Global.clan_spawned.emit()
	if !Global.cousin_clans.has(global_position):
		Global.cousin_clans[global_position] = true
		clan_name = Global.pick_clan_name()
		# #print("adding clan ", clan_name, " ", global_position)

	var color = Global.pick_clan_color()
	
	for i in range(Global.cousins_per_clan):
		var new_fish = fish_preload.instantiate()
		var angle = TAU * i / Global.cousins_per_clan
		new_fish.position = praying_radius * Vector2.from_angle(angle)
		new_fish.linear_velocity = 1000 * Vector2.from_angle(angle + TAU / 3)
		new_fish.rotation = angle
		new_fish.set_color(color)
		$fishes.add_child(new_fish)

func _process(delta):
	var all_fishes = $fishes.get_children().filter(func(body): return body is Fish)
	
	if not all_collected and all_fishes.size() == 0:
		all_collected = true
		Global.clan_collected.emit()
		SoundManager.get_clan()
		Global.cousin_clans[global_position] = false
		hide()
		await create_tween().tween_method(func(t): $Voice.volume_db = t, BASE_VOLUME, -30., 3.).finished
		call_deferred("queue_free")
	
	for fish in all_fishes:
		var far = -fish.position.length() / float(praying_radius / 2.)
		# Stay within radius
		fish.apply_central_force(delta * 60 * praying_radius / 2. * far ** 5. * fish.position.normalized())
		# Goes towards perimeter
		fish.apply_central_force(delta * 60_000 * fish.position.normalized())
		# Rotates
		fish.apply_central_force(delta * 54_000 * Vector2.from_angle(fish.rotation))
		fish.apply_torque(delta * -6000)
		
		var converted_fishes = []
		var fish_chool
		for voisin in fish.find_child("RepulseArea").get_overlapping_bodies()\
			.filter(func(body): return body is Fish and body.is_swimming):
			if not converted_fishes.has(fish):
				converted_fishes.push_back(fish)
				fish_chool = voisin.get_parent()
		
		for converted_fish in converted_fishes:
			$fishes.remove_child(fish)
			fish_chool.add_child(converted_fish)
			converted_fish.is_swimming = true
			converted_fish.position += get_global_position()
