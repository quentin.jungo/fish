extends Control

@onready var particles = preload("res://src/collectables/fish_clans/clan_particles.tscn")


func _process(_delta):
	for clan in Global.cousin_clans:
		var current_clan = null
		var my_children = get_children()
		for child in my_children: # should only have particle children
			if child.clan_location == clan:
				current_clan = child
				break
		if !current_clan:
			var new_clan = particles.instantiate()
			new_clan.assign_clan(clan)
			add_child(new_clan)
		else:
			var dist = Global.camera.global_position - current_clan.clan_location
			current_clan.scale_amount_max = clampf(remap(dist.length(), 10000, 0, 0.8, 3), 0.8, 3)
			current_clan.position = -dist
			current_clan.direction = dist.normalized()
			current_clan.position.x = clamp(current_clan.position.x, -position.x, position.x)
			current_clan.position.y = clamp(current_clan.position.y, -position.y, position.y)
			current_clan.emitting = Global.cousin_clans[clan]