extends Node2D
class_name Ocean

##
@onready var fish_school = $FishSchool
## The chunk to load when lauching the scene. (Only for dbg)
@export var initial_chunk_to_load: int = 0
## The index of the chunk we are in.
var chunks: Dictionary # < chunk number ; chunk node >
##
var fish_net_preload = preload("res://src/ennemies/fish_net/fish_net.tscn")
##
var fish_net_timer = Timer.new()
##
var entered_chunk_001 = false
## The distance away from a new chunk to populate it.
const DISTANCE_TO_LOAD: int = 500
##
const CHUNK_SIZE_X: int = 8000
##
const SPAWN_OFFSET: Vector2i = Vector2i(-500, 500)


func _ready():
	SoundManager.stop_menu_music()

	_load_chunk(initial_chunk_to_load)
	fish_net_timer.wait_time = 1.2
	Global.win.connect(clean_all_chunks)
	fish_net_timer.timeout.connect(_spawn_fish_net)
	fish_net_timer.autostart = true
	add_child(fish_net_timer)
	fish_net_timer.start()
	if initial_chunk_to_load > 0:
		_load_chunk(initial_chunk_to_load - 1)
	if initial_chunk_to_load != 0:
		Global.end_intro.emit()
		Global.end_intro_fish_move.emit()
		Global.playing_status = Global.SWIMMING
		SoundManager.start_music()

	else:
		$InGameMenu/boussole.hide()


func _process(_delta):
	var fish_school_position_x = roundi(Global.camera.global_position.x)
	var sub_pos = fish_school_position_x % CHUNK_SIZE_X
	var current_chunk = _get_current_chunk()
	if sub_pos > CHUNK_SIZE_X - 3000:
		_load_chunk(current_chunk + 1)
	if sub_pos < 1000 and current_chunk - 1 >= 0:
		_load_chunk(current_chunk - 1)
	if not entered_chunk_001 and current_chunk >= 1:
		entered_chunk_001 = true
		$InGameMenu/boussole.show()
		#print(Global.is_first_play)
		if Global.is_first_play and not SoundManager.find_child("Compass").playing:
			SoundManager.compass_tuto()
		if not SoundManager.is_music_playing():
			SoundManager.start_music()



func _get_current_chunk() -> int:
	return int(Global.camera.position.x / CHUNK_SIZE_X) - initial_chunk_to_load

## Load the given chunk if not already loaded. Side effect : call _clean_chuncks()
func _load_chunk(chunk_to_load):
	_clean_chunks()
	if chunks.has(chunk_to_load):
		# #print("already loaded")
		return
	if Global.CHUNKS.size() <= chunk_to_load || chunk_to_load < 0:
		# #print("LE CHUNK ", chunk_to_load, " n'existe PAS" )
		return
	var chunk_load = Global.CHUNKS[chunk_to_load]
	var chunk = chunk_load.instantiate()
	chunk.position = SPAWN_OFFSET
	chunk.position.x += (chunk_to_load - initial_chunk_to_load) * CHUNK_SIZE_X
	#print("pos: ", chunk.position.x, " for ", chunk_to_load)
	add_child(chunk)
	chunks[chunk_to_load] = chunk

## Will check if some chunks are enought away to free them and perfom if possible
func _clean_chunks():
	var current_chunk = _get_current_chunk()
	for chunk_n in chunks.keys():
		if chunk_n > current_chunk - 2:
			continue
		_erase_chunk(chunk_n)

## Will erase the given chunk in the node tree and in the dictionnary 	
func _erase_chunk(chunk_number: int):
	if !chunks.has(chunk_number):
		return
	chunks[chunk_number].queue_free()
	chunks.erase(chunk_number)

## Spawn a fish net if the camera is in the fishing zone
func _spawn_fish_net():
	if not Global.is_in_fishing_zone:
		return
	var fish_net = fish_net_preload.instantiate()
	fish_net.position = Global.camera.position
	add_child(fish_net)

##
func clean_all_chunks():
	for chunk_n in chunks.keys():
		if chunk_n == 9:
			continue
		_erase_chunk(chunk_n)
