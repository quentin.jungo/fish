extends Node2D
class_name MouseLight

@onready var light: Light2D = $MouseLight
@onready var zone: Area2D = $LightZone
@onready var col: CharacterBody2D = $COL

var last_position: Vector2
var last_position_1: Vector2

func _ready():
	visible = false
	Global.lightZone = zone
	Global.win.connect(_on_win)

func _process(_delta):
	if Global.playing_status != Global.SWIMMING:
		return
	visible = true
	var position_to_go: Vector2 = Vector2.ZERO
	var fish_school = get_parent()
	match fish_school.input_mode:
		fish_school.MOUSE:
			position_to_go = get_global_mouse_position()
		fish_school.JOYPAD:
			position_to_go = fish_school.joy_position

	col.global_position = position_to_go

	if fish_school.is_light_freeze:
		light.position = last_position_1
		zone.position = last_position_1
		return
	last_position_1 = last_position
	last_position = position_to_go
	light.position = position_to_go
	zone.position = position_to_go

func _on_win():
	visible = true
	await create_tween().tween_method(func(t): light.energy = t, 1., 0., 3).finished
	queue_free()
