extends Polygon2D

@onready var collisionShape = $StaticBody2D/CollisionPolygon2D
@onready var contour = $Line2D

@export var line_sprite : Texture = preload("res://assets/texture/floor_texturesv2.png")

var lightOccluder: LightOccluder2D = LightOccluder2D.new()
var area: Area2D = Area2D.new()

func _ready():
	add_child(lightOccluder)
	lightOccluder.occluder = OccluderPolygon2D.new()
	collisionShape.polygon = polygon
	lightOccluder.occluder.polygon = polygon
	var foo = polygon
	if foo.size() > 1:
		foo.append(foo[0])
	contour.points = foo
	contour.texture = line_sprite
	add_child(area)
	area.add_child(collisionShape.duplicate())

	area.body_entered.connect(_on_entered)
	area.body_exited.connect(_on_exited)
	area.set_collision_mask_value(12, true)

func _on_entered(body: Node2D):
	if body is CharacterBody2D and body.name == "COL":
		var fish_school = find_parent("Ocean").find_child("FishSchool")
		fish_school.light_is_in_obstacle.push_back(self)

func _on_exited(body: Node2D):
	if body is CharacterBody2D and body.name == "COL":
		var fish_school = find_parent("Ocean").find_child("FishSchool")
		fish_school.light_is_in_obstacle.erase(self)

