extends Node2D

@onready var plankton_tex = preload("res://assets/texture/plankton_spritesheet_1x8.png")
@onready var plankton = $plankton


func _ready():
	for i in range(0, 8):
		var pl = plankton.duplicate()
		pl.texture = plankton.texture.duplicate()
		pl.texture.region = Rect2(i*32, 0, 32, 32)
		add_child(pl)
