@tool
extends CPUParticles2D


@onready var texture_raw = preload("res://assets/texture/ocean-particles-1x8.png")

var particles = AnimatedTexture.new()
var t = Array()

func _ready():
	particles.frames = 8
	for i in range(8):
		var tex = AtlasTexture.new()
		tex.atlas = texture_raw
		tex.region = Rect2(0+i*32, 0, 32, 32)
		t.append(tex)
		#print(tex)
		#print(i)
		particles.set_frame_texture(i, t.back())
#	self.texture = 
