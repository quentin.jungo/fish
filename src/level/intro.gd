extends Node2D

@onready var intro_fade: ColorRect = $IntroFade
@onready var move_message: Node2D = $Move
var _one_dead = false
@onready var intro_voice: AudioStreamPlayer = SoundManager.find_child("IntroP1")
@onready var intro_music: AudioStreamPlayer = SoundManager.find_child("Piano")
var _first_sec_passed = false


func _ready():
	_set_first_sec()
	Global.playing_status = Global.INTRO # Praying
	move_message.modulate.a = 0

	Global.death.connect(_on_death)
	Global.go_to_menu.connect(func(): queue_free())

	intro_music.play()
	intro_voice.play()

	intro_fade.color = Color.BLACK
	await create_tween().tween_method(func(t): intro_fade.color.a = t, 1., 0., 3).finished
	intro_fade.queue_free()
	
	await intro_voice.finished
	Global.playing_status = Global.INTRO_END
	create_tween().tween_method(func(t): move_message.modulate.a = t, 0., 1., 0.7)


func _set_first_sec():
	await get_tree().create_timer(1).timeout
	_first_sec_passed = true


func _input(event):
	if not _first_sec_passed:
		return
	if event is InputEventMouseMotion \
	or not Input.get_vector("pad1_down", "pad1_left", "pad1_right", "pad1_up").is_zero_approx() \
	or not Input.get_vector("pad2_down", "pad2_left", "pad2_right", "pad2_up").is_zero_approx():
		match Global.playing_status:
			Global.INTRO:
				pass
			Global.INTRO_END:
				Global.playing_status = Global.SWIMMING
				Global.end_intro_fish_move.emit()

	if event is InputEventJoypadButton or event is InputEventKey or event is InputEventMouseButton:
		match Global.playing_status:
			Global.INTRO:
				if Global.is_first_play:
					return
				kill_intro()
				Global.playing_status = Global.SWIMMING
			Global.INTRO_END:
				Global.playing_status = Global.SWIMMING
				Global.end_intro_fish_move.emit()


func kill_intro():
	intro_voice.stop()
	intro_music.stop()
	SoundManager.start_music()
	Global.end_intro.emit()
	Global.end_intro_fish_move.emit()
	queue_free()


func _on_death(_name, _color):
	if _one_dead:
		return
	_one_dead = true
	intro_music.stop()
	var intro_voice2: AudioStreamPlayer = SoundManager.find_child("IntroP2")
	intro_voice2.play()
	await intro_voice2.finished
	queue_free()


