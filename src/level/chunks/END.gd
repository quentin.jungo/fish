extends Node2D

@onready var area = $Area2D


func _ready():
	area.body_entered.connect(_on_body_entered)


func _on_body_entered(body):
	if body is Fish:
		# var parent = get_parent()
		# parent.get_children().filter(func(child): return child is AlgueZone).map(func(x): x.queue_free())
		SoundManager.end()
		area.queue_free()
		Global.win.emit()
		var canvas = body.find_parent("Ocean").find_child("CanvasModulate")
		await create_tween().tween_method(func(t): canvas.color.v = t, 0., 1., 3).finished
		Global.playing_status = Global.END
		canvas.queue_free()
