extends Node2D

func _ready():
	var a = find_child("spawn_zone")
	var b = find_child("size")
	var c = find_child("max")
	if a: a.visible = false
	if b: b.visible = false
	if c: c.visible = false

