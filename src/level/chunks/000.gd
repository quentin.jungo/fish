extends Node2D

var introload = preload("res://src/level/intro.tscn")

func _ready():
	var intro: Node2D = introload.instantiate()
	intro.position = Vector2i(500, -500)
	add_child(intro)
	#print("add intro !!", intro.name)
	Global.end_intro.connect(func(): _free_background(null, null); _free_intro())
	Global.death.connect(_free_background)

func _free_background(_name, _color):
	var b = find_child("Lights")
	if b:
		b.queue_free()

func _free_intro():
	var b = find_child("Intro")
	if b:
		b.queue_free()
